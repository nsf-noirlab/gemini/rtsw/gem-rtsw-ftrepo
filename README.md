Setup
========

Select your branch, Eg., crcs_cp

Machine [5] shown in ![ALT](ADE2Machines.png)

    ssh <machine [5]>
    git clone -b crcs_cp git@gitlab.com:nsf-noirlab/gemini/rtsw/gem-rtsw-ftrepo.git
    cd gem-rtsw-ftrepo.git
    sudo cp *.repo /etc/yum.repo.d/
    sudo dnf makecache
    sudo dnf install crcs_cp-devel


    
